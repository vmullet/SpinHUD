<?php

class baseDevice {

    protected $_name;
    protected $_count;

    /**
     * baseDevice constructor.
     * @param $_name
     * @param $_count
     */
    public function __construct($_name, $_count)
    {
        $this->_name = $_name;
        $this->_count = $_count;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->_count = $count;
    }



}


?>