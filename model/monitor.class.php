<?php

require(dirname(__FILE__,2).'/vendor/autoload.php');
require('vpsSystem.class.php');

$client = new GuzzleHttp\Client();

class monitor {

    private static $_instance = null;
    const URL_SERVER = 'http://vps.lostarchives.fr/phpsysinfo/xml.php?plugin=complete&json'; // URL TO request phpsysinfo json

    private function __construct()
    {
    }

    public static function getInstance() {

        if (!isset(self::$_instance)) {
            self::$_instance = new monitor();
        }

        return self::$_instance;

    }

    /**
     * @return vpsSystem
     */
    public function getSystem() {

        global $client;

        $response = $client->get(self::URL_SERVER);

        $response = json_decode($response->getBody()->getContents(),true);

        //var_dump($response);

        $network_devices = array();
        foreach($response['Network']['NetDevice'] as $raw_netcard) {
            $netcard = new networkDevice(
                $raw_netcard['@attributes']['Name'],
                $raw_netcard['@attributes']['RxBytes'],
                $raw_netcard['@attributes']['TxBytes'],
                $raw_netcard['@attributes']['Err'],
                $raw_netcard['@attributes']['Drops'],
                $raw_netcard['@attributes']['Info']
            );

            $network_devices[] = $netcard;
        }


        $pci_devices = array();
        foreach($response['Hardware']['PCI']['Device'] as $raw_pcicard) {
            $pcicard = new baseDevice(
                $raw_pcicard['@attributes']['Name'],
                $raw_pcicard['@attributes']['Count']
            );

            $pci_devices[] = $pcicard;
        }



        $usb_devices = array();
        foreach($response['Hardware']['USB']['Device'] as $raw_usbcard) {
            $usbcard = new baseDevice(
                $raw_usbcard['@attributes']['Name'],
                $raw_usbcard['@attributes']['Count']
            );

            $usb_devices[] = $usbcard;
        }


        $i2c_devices = array();
        foreach($response['Hardware']['I2C']['Device'] as $raw_i2ccard) {

            $i2ccard = new baseDevice(
                $raw_i2ccard['Name'],
                $raw_i2ccard['Count']
            );

            $i2c_devices[] = $i2ccard;
        }

        $hdd_devices = array();
        foreach($response['FileSystem']['Mount'] as $raw_hdd) {
            $hdd_device = new hddDevice(
                $raw_hdd['@attributes']['Name'],
                $raw_hdd['@attributes']['MountPointID'],
                $raw_hdd['@attributes']['FSType'],
                $raw_hdd['@attributes']['Free'],
                $raw_hdd['@attributes']['Used'],
                $raw_hdd['@attributes']['Total'],
                $raw_hdd['@attributes']['Percent'],
                $raw_hdd['@attributes']['MountPoint']

            );

            $hdd_devices[] = $hdd_device;

        }

        $cpu = new cpu(
            $response['Hardware']['CPU']['CpuCore']['@attributes']['Model'],
            $response['Hardware']['CPU']['CpuCore']['@attributes']['CpuSpeed'],
            $response['Hardware']['CPU']['CpuCore']['@attributes']['Cache'],
            $response['Hardware']['CPU']['CpuCore']['@attributes']['Virt'],
            $response['Hardware']['CPU']['CpuCore']['@attributes']['Bogomips']
        );

        $memory = new memory(
            $response['Memory']['@attributes']['Free'],
            $response['Memory']['@attributes']['Used'],
            $response['Memory']['@attributes']['Total'],
            $response['Memory']['@attributes']['Percent'],
            $response['Memory']['Details']['@attributes']['App'],
            $response['Memory']['Details']['@attributes']['AppPercent'],
            $response['Memory']['Details']['@attributes']['Buffers'],
            $response['Memory']['Details']['@attributes']['BuffersPercent'],
            $response['Memory']['Details']['@attributes']['Cached'],
            $response['Memory']['Details']['@attributes']['CachedPercent']
        );

        $processes = array();
        foreach($response['Plugins']['Plugin_PS']['Process'] as $raw_process) {
            $process = new process(
                $raw_process['@attributes']['Name'],
                $raw_process['@attributes']['PID'],
                $raw_process['@attributes']['PPID'],
                $raw_process['@attributes']['ParentID'],
                $raw_process['@attributes']['MemoryUsage']
            );

            $processes[] = $process;

        }


        $hardware = new hardware(
            $response['Hardware']['@attributes']['Name'],
            $network_devices,
            $pci_devices,
            $usb_devices,
            $i2c_devices,
            $hdd_devices,
            $cpu,
            $memory,
            $processes
        );

        $sys = new vpsSystem(
            $response['Vitals']['@attributes']['Hostname'],
            $response['Vitals']['@attributes']['IPAddr'],
            $response['Vitals']['@attributes']['Kernel'],
            $response['Vitals']['@attributes']['Distro'],
            $response['Vitals']['@attributes']['Uptime'],
            $response['Vitals']['@attributes']['Users'],
            $response['Vitals']['@attributes']['LoadAvg'],
            $response['Vitals']['@attributes']['SysLang'],
            $response['Vitals']['@attributes']['Processes'],
            $response['Vitals']['@attributes']['ProcessesRunning'],
            $response['Vitals']['@attributes']['OS'],
            $hardware

        );
        //var_dump($sys);
        return $sys;

    }


}




?>