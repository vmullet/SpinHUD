<?php

class cpu {

    private $_model;
    private $_cpuSpeed;
    private $_cacheValue;
    private $_virtTech;
    private $_bogoMips;

    /**
     * cpu constructor.
     * @param $_model
     * @param $_cpuSpeed
     * @param $_cacheValue
     * @param $_virtTech
     * @param $_bogoMips
     */
    public function __construct($_model, $_cpuSpeed, $_cacheValue, $_virtTech, $_bogoMips)
    {
        $this->_model = $_model;
        $this->_cpuSpeed = $_cpuSpeed;
        $this->_cacheValue = $_cacheValue;
        $this->_virtTech = $_virtTech;
        $this->_bogoMips = $_bogoMips;
    }


    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->_model = $model;
    }

    /**
     * @return mixed
     */
    public function getCpuSpeed()
    {
        return round($this->_cpuSpeed/1000,2);
    }

    /**
     * @param mixed $cpuSpeed
     */
    public function setCpuSpeed($cpuSpeed)
    {
        $this->_cpuSpeed = $cpuSpeed;
    }

    /**
     * @return mixed
     */
    public function getCacheValue()
    {
        return $this->_cacheValue/pow(1024,2);
    }

    /**
     * @param mixed $cacheValue
     */
    public function setCacheValue($cacheValue)
    {
        $this->_cacheValue = $cacheValue;
    }

    /**
     * @return mixed
     */
    public function getVirtTech()
    {
        return $this->_virtTech;
    }

    /**
     * @param mixed $virtTech
     */
    public function setVirtTech($virtTech)
    {
        $this->_virtTech = $virtTech;
    }

    /**
     * @return mixed
     */
    public function getBogoMips()
    {
        return $this->_bogoMips;
    }

    /**
     * @param mixed $bogoMips
     */
    public function setBogoMips($bogoMips)
    {
        $this->_bogoMips = $bogoMips;
    }



}


?>