<?php

class process {

    private $_name;
    private $_pid;
    private $_ppid;
    private $_parent;
    private $_memoryUsage;

    /**
     * process constructor.
     * @param $_name
     * @param $_pid
     * @param $_ppid
     * @param $_parent
     * @param $_memoryUsage
     */
    public function __construct($_name, $_pid, $_ppid, $_parent, $_memoryUsage)
    {
        $this->_name = $_name;
        $this->_pid = $_pid;
        $this->_ppid = $_ppid;
        $this->_parent = $_parent;
        $this->_memoryUsage = $_memoryUsage;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->_pid;
    }

    /**
     * @param mixed $pid
     */
    public function setPid($pid)
    {
        $this->_pid = $pid;
    }

    /**
     * @return mixed
     */
    public function getPpid()
    {
        return $this->_ppid;
    }

    /**
     * @param mixed $ppid
     */
    public function setPpid($ppid)
    {
        $this->_ppid = $ppid;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->_parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->_parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getMemoryUsage()
    {
        return $this->_memoryUsage;
    }

    /**
     * @param mixed $memoryUsage
     */
    public function setMemoryUsage($memoryUsage)
    {
        $this->_memoryUsage = $memoryUsage;
    }





}



?>