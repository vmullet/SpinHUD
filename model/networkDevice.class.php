<?php

class networkDevice extends baseDevice {

    private $_rxBytes;
    private $_txBytes;
    private $_err;
    private $_drops;
    private $_info;

    /**
     * networkDevice constructor.
     * @param $_rxBytes
     * @param $_txBytes
     * @param $_err
     * @param $_drops
     * @param $_info
     */
    public function __construct($_name,$_rxBytes, $_txBytes, $_err, $_drops, $_info)
    {
        parent::__construct($_name,1);
        $this->_rxBytes = $_rxBytes;
        $this->_txBytes = $_txBytes;
        $this->_err = $_err;
        $this->_drops = $_drops;
        $this->_info = $_info;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->_count = $count;
    }



    /**
     * @return mixed
     */
    public function getRxBytes()
    {
        return round($this->_rxBytes/pow(1024,2),0);
    }

    /**
     * @param mixed $rxBytes
     */
    public function setRxBytes($rxBytes)
    {
        $this->_rxBytes = $rxBytes;
    }

    /**
     * @return mixed
     */
    public function getTxBytes()
    {
        return round($this->_txBytes/pow(1024,2),0);
    }

    /**
     * @param mixed $txBytes
     */
    public function setTxBytes($txBytes)
    {
        $this->_txBytes = $txBytes;
    }

    /**
     * @return mixed
     */
    public function getErr()
    {
        return $this->_err;
    }

    /**
     * @param mixed $err
     */
    public function setErr($err)
    {
        $this->_err = $err;
    }

    /**
     * @return mixed
     */
    public function getDrops()
    {
        return $this->_drops;
    }

    /**
     * @param mixed $drops
     */
    public function setDrops($drops)
    {
        $this->_drops = $drops;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->_info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->_info = $info;
    }



}

?>