<?php

require('baseDevice.class.php');
require('cpu.class.php');
require('hardware.class.php');
require('hddDevice.class.php');
require('memory.class.php');
require('networkDevice.class.php');
require('process.class.php');



class vpsSystem {

    private $_hostname;
    private $_ipAddress;
    private $_kernel;
    private $_distro;
    private $_upTime;
    private $_nbUsers;
    private $_loadAvr;
    private $_sysLang;
    private $_totalProcesses;
    private $_runningProcesses;
    private $_os;

    private $_hardware;

    /**
     * system constructor.
     * @param $_hostname
     * @param $_ipAddress
     * @param $_kernel
     * @param $_distro
     * @param $_upTime
     * @param $_nbUsers
     * @param $_loadAvr
     * @param $_sysLang
     * @param $_totalProcesses
     * @param $_runningProcesses
     * @param $_os
     * @param $_hardware
     */
    public function __construct($_hostname, $_ipAddress, $_kernel, $_distro, $_upTime, $_nbUsers, $_loadAvr, $_sysLang, $_totalProcesses, $_runningProcesses, $_os, $_hardware)
    {
        $this->_hostname = $_hostname;
        $this->_ipAddress = $_ipAddress;
        $this->_kernel = $_kernel;
        $this->_distro = $_distro;
        $this->_upTime = $_upTime;
        $this->_nbUsers = $_nbUsers;
        $this->_loadAvr = $_loadAvr;
        $this->_sysLang = $_sysLang;
        $this->_totalProcesses = $_totalProcesses;
        $this->_runningProcesses = $_runningProcesses;
        $this->_os = $_os;
        $this->_hardware = $_hardware;
    }


    /**
     * @return mixed
     */
    public function getHostname()
    {
        return $this->_hostname;
    }

    /**
     * @param mixed $hostname
     */
    public function setHostname($hostname)
    {
        $this->_hostname = $hostname;
    }

    /**
     * @return mixed
     */
    public function getIpAddress()
    {
        return $this->_ipAddress;
    }

    /**
     * @param mixed $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->_ipAddress = $ipAddress;
    }

    /**
     * @return mixed
     */
    public function getKernel()
    {
        return $this->_kernel;
    }

    /**
     * @param mixed $kernel
     */
    public function setKernel($kernel)
    {
        $this->_kernel = $kernel;
    }

    /**
     * @return mixed
     */
    public function getDistro()
    {
        return $this->_distro;
    }

    /**
     * @param mixed $distro
     */
    public function setDistro($distro)
    {
        $this->_distro = $distro;
    }

    /**
     * @return mixed
     */
    public function getUpTime()
    {
        return $this->_upTime;
    }

    /**
     * @param mixed $upTime
     */
    public function setUpTime($upTime)
    {
        $this->_upTime = $upTime;
    }

    /**
     * @return mixed
     */
    public function getNbUsers()
    {
        return $this->_nbUsers;
    }

    /**
     * @param mixed $nbUsers
     */
    public function setNbUsers($nbUsers)
    {
        $this->_nbUsers = $nbUsers;
    }

    /**
     * @return mixed
     */
    public function getLoadAvr()
    {
        return $this->_loadAvr;
    }

    /**
     * @param mixed $loadAvr
     */
    public function setLoadAvr($loadAvr)
    {
        $this->_loadAvr = $loadAvr;
    }

    /**
     * @return mixed
     */
    public function getSysLang()
    {
        return $this->_sysLang;
    }

    /**
     * @param mixed $sysLang
     */
    public function setSysLang($sysLang)
    {
        $this->_sysLang = $sysLang;
    }

    /**
     * @return mixed
     */
    public function getTotalProcesses()
    {
        return $this->_totalProcesses;
    }

    /**
     * @param mixed $totalProcesses
     */
    public function setTotalProcesses($totalProcesses)
    {
        $this->_totalProcesses = $totalProcesses;
    }

    /**
     * @return mixed
     */
    public function getRunningProcesses()
    {
        return $this->_runningProcesses;
    }

    /**
     * @param mixed $runningProcesses
     */
    public function setRunningProcesses($runningProcesses)
    {
        $this->_runningProcesses = $runningProcesses;
    }

    /**
     * @return mixed
     */
    public function getOs()
    {
        return $this->_os;
    }

    /**
     * @param mixed $os
     */
    public function setOs($os)
    {
        $this->_os = $os;
    }

    /**
     * @return hardware
     */
    public function getHardware() : hardware
    {
        return $this->_hardware;
    }

    /**
     * @param hardware $hardware
     */
    public function setHardware($hardware)
    {
        $this->_hardware = $hardware;
    }





}


?>