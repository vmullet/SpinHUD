<?php

class memory {

    private $_freeAmount;
    private $_usedAmount;
    private $_totalAmount;
    private $_usedPercent;

    private $_appConsumption;
    private $_appConsumptionPercent;
    private $_buffersConsumption;
    private $_buffersConsumptionPercent;
    private $_cacheConsumption;
    private $_cacheConsumptionPercent;

    /**
     * memory constructor.
     * @param $_freeAmount
     * @param $_usedAmount
     * @param $_totalAmount
     * @param $_usedPerdent
     * @param $_appConsumption
     * @param $_appConsumptionPercent
     * @param $_buffersConsumption
     * @param $_buffersConsumptionPercent
     * @param $_cacheConsumption
     * @param $_cacheConsumptionPercent
     */
    public function __construct($_freeAmount, $_usedAmount, $_totalAmount, $_usedPerdent, $_appConsumption, $_appConsumptionPercent, $_buffersConsumption, $_buffersConsumptionPercent, $_cacheConsumption, $_cacheConsumptionPercent)
    {
        $this->_freeAmount = $_freeAmount;
        $this->_usedAmount = $_usedAmount;
        $this->_totalAmount = $_totalAmount;
        $this->_usedPercent = $_usedPerdent;
        $this->_appConsumption = $_appConsumption;
        $this->_appConsumptionPercent = $_appConsumptionPercent;
        $this->_buffersConsumption = $_buffersConsumption;
        $this->_buffersConsumptionPercent = $_buffersConsumptionPercent;
        $this->_cacheConsumption = $_cacheConsumption;
        $this->_cacheConsumptionPercent = $_cacheConsumptionPercent;
    }


    /**
     * @return mixed
     */
    public function getFreeAmount()
    {
        return round($this->_freeAmount/pow(1024,2),0);
    }

    /**
     * @param mixed $freeAmount
     */
    public function setFreeAmount($freeAmount)
    {
        $this->_freeAmount = $freeAmount;
    }

    /**
     * @return mixed
     */
    public function getUsedAmount()
    {
        return round($this->_usedAmount/pow(1024,2),0);
    }

    /**
     * @param mixed $usedAmount
     */
    public function setUsedAmount($usedAmount)
    {
        $this->_usedAmount = $usedAmount;
    }

    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return round($this->_totalAmount/pow(1024,3),2);
    }

    /**
     * @param mixed $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->_totalAmount = $totalAmount;
    }

    /**
     * @return mixed
     */
    public function getUsedPercent()
    {
        return $this->_usedPercent;
    }

    /**
     * @param mixed $usedPerdent
     */
    public function setUsedPercent($usedPerdent)
    {
        $this->_usedPercent = $usedPerdent;
    }

    /**
     * @return mixed
     */
    public function getAppConsumption()
    {
        return $this->_appConsumption;
    }

    /**
     * @param mixed $appConsumption
     */
    public function setAppConsumption($appConsumption)
    {
        $this->_appConsumption = $appConsumption;
    }

    /**
     * @return mixed
     */
    public function getAppConsumptionPercent()
    {
        return $this->_appConsumptionPercent;
    }

    /**
     * @param mixed $appConsumptionPercent
     */
    public function setAppConsumptionPercent($appConsumptionPercent)
    {
        $this->_appConsumptionPercent = $appConsumptionPercent;
    }

    /**
     * @return mixed
     */
    public function getBuffersConsumption()
    {
        return $this->_buffersConsumption;
    }

    /**
     * @param mixed $buffersConsumption
     */
    public function setBuffersConsumption($buffersConsumption)
    {
        $this->_buffersConsumption = $buffersConsumption;
    }

    /**
     * @return mixed
     */
    public function getBuffersConsumptionPercent()
    {
        return $this->_buffersConsumptionPercent;
    }

    /**
     * @param mixed $buffersConsumptionPercent
     */
    public function setBuffersConsumptionPercent($buffersConsumptionPercent)
    {
        $this->_buffersConsumptionPercent = $buffersConsumptionPercent;
    }

    /**
     * @return mixed
     */
    public function getCacheConsumption()
    {
        return round($this->_cacheConsumption/pow(1024,2),0);
    }

    /**
     * @param mixed $cacheConsumption
     */
    public function setCacheConsumption($cacheConsumption)
    {
        $this->_cacheConsumption = $cacheConsumption;
    }

    /**
     * @return mixed
     */
    public function getCacheConsumptionPercent()
    {
        return $this->_cacheConsumptionPercent;
    }

    /**
     * @param mixed $cacheConsumptionPercent
     */
    public function setCacheConsumptionPercent($cacheConsumptionPercent)
    {
        $this->_cacheConsumptionPercent = $cacheConsumptionPercent;
    }




}


?>