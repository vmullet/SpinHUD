<?php

class hardware {

    private $_label;

    private $_networkDevices;
    private $_pciDevices; // baseDevice
    private $_usbDevices; // baseDevice
    private $_i2cDevices; // baseDevice

    private $_cpu;
    private $_memory;

    private $_hddDevices;
    private $_processes;

    /**
     * hardware constructor.
     * @param $_label
     * @param $_networkDevices
     * @param $_pciDevices
     * @param $_usbDevices
     * @param $_i2cDevices
     * @param $_cpu
     * @param $_memory
     * @param $_hddDevices
     * @param $_processes
     */
    public function __construct($_label, $_networkDevices, $_pciDevices, $_usbDevices, $_i2cDevices, $_hddDevices, $_cpu, $_memory, $_processes)
    {
        $this->_label = $_label;
        $this->_networkDevices = $_networkDevices;
        $this->_pciDevices = $_pciDevices;
        $this->_usbDevices = $_usbDevices;
        $this->_i2cDevices = $_i2cDevices;
        $this->_hddDevices = $_hddDevices;
        $this->_cpu = $_cpu;
        $this->_memory = $_memory;
        $this->_processes = $_processes;
    }


    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->_label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->_label = $label;
    }

    /**
     * @return networkDevice[]
     */
    public function getNetworkDevices()
    {
        return $this->_networkDevices;
    }

    /**
     * @param mixed $networkDevices
     */
    public function setNetworkDevices($networkDevices)
    {
        $this->_networkDevices = $networkDevices;
    }

    /**
     * @return baseDevice[]
     */
    public function getPciDevices()
    {
        return $this->_pciDevices;
    }

    /**
     * @param mixed $pciDevices
     */
    public function setPciDevices($pciDevices)
    {
        $this->_pciDevices = $pciDevices;
    }

    /**
     * @return baseDevice[]
     */
    public function getUsbDevices()
    {
        return $this->_usbDevices;
    }

    /**
     * @param mixed $usbDevices
     */
    public function setUsbDevices($usbDevices)
    {
        $this->_usbDevices = $usbDevices;
    }

    /**
     * @return baseDevice[]
     */
    public function getI2cDevices()
    {
        return $this->_i2cDevices;
    }

    /**
     * @param mixed $i2cDevices
     */
    public function setI2cDevices($i2cDevices)
    {
        $this->_i2cDevices = $i2cDevices;
    }

    /**
     * @return mixed
     */
    public function getCpu() : cpu
    {
        return $this->_cpu;
    }

    /**
     * @param mixed $cpu
     */
    public function setCpu($cpu)
    {
        $this->_cpu = $cpu;
    }

    /**
     * @return memory
     */
    public function getMemory() : memory
    {
        return $this->_memory;
    }

    /**
     * @param memory $memory
     */
    public function setMemory($memory)
    {
        $this->_memory = $memory;
    }

    /**
     * @return hddDevice[]
     */
    public function getHddDevices()
    {
        return $this->_hddDevices;
    }

    /**
     * @param hddDevice[] $hddDevices
     */
    public function setHddDevices($hddDevices)
    {
        $this->_hddDevices = $hddDevices;
    }

    /**
     * @return process[]
     */
    public function getProcesses()
    {
        return $this->_processes;
    }

    /**
     * @param process[] $processes
     */
    public function setProcesses($processes)
    {
        $this->_processes = $processes;
    }




}


?>