<?php

class hddDevice extends baseDevice {

    private $_mountPointId;
    private $_fsType;
    private $_freeAmount;
    private $_usedAmount;
    private $_totalAmount;
    private $_usedAmountPercent;
    private $_mountPoint;

    /**
     * hddDevice constructor.
     * @param $_mountPointId
     * @param $_fsType
     * @param $_freeAmount
     * @param $_usedAmount
     * @param $_totalAmount
     * @param $_usedAmountPercent
     * @param $_mountPoint
     */
    public function __construct($_name,$_mountPointId, $_fsType, $_freeAmount, $_usedAmount, $_totalAmount, $_usedAmountPercent, $_mountPoint)
    {
        parent::__construct($_name,1);
        $this->_mountPointId = $_mountPointId;
        $this->_fsType = $_fsType;
        $this->_freeAmount = $_freeAmount;
        $this->_usedAmount = $_usedAmount;
        $this->_totalAmount = $_totalAmount;
        $this->_usedAmountPercent = $_usedAmountPercent;
        $this->_mountPoint = $_mountPoint;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }


    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->_count;
    }


    /**
     * @return mixed
     */
    public function getMountPointId()
    {
        return $this->_mountPointId;
    }



    /**
     * @return mixed
     */
    public function getFsType()
    {
        return $this->_fsType;
    }


    /**
     * @return mixed
     */
    public function getFreeAmount()
    {
        return round($this->_freeAmount/pow(1024,3),2);
    }



    /**
     * @return mixed
     */
    public function getUsedAmount()
    {
        return round($this->_usedAmount/pow(1024,3),2);
    }



    /**
     * @return mixed
     */
    public function getTotalAmount()
    {
        return round($this->_totalAmount/pow(1024,3),2);
    }


    /**
     * @return mixed
     */
    public function getUsedAmountPercent()
    {
        return $this->_usedAmountPercent;
    }

    public function getUsedAmountRadius() {

        return round($this->_usedAmountPercent*7/100,2);
    }


    /**
     * @return mixed
     */
    public function getMountPoint()
    {
        return $this->_mountPoint;
    }


}


?>