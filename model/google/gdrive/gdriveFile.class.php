<?php 

class gdriveFile {

	private $_id;
	private $_elementId;
	private $_name;
	private $_description;
	private $_mimeType;
	private $_isFolder;
    private $_viewLink;
    private $_downloadLink;
    private $_thumbnailLink;
    private $_iconLink;
    private $_shared;
    private $_starred;
    private $_fullExtension;
    private $_size;
    private $_createdTime;
    private $_modifiedTime;
	private $_owners;
	private $_permissions;
	private $_lastUserModifiyer;

	public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getElementId()
    {
        return $this->_elementId;
    }

    /**
     * @param mixed $elementId
     */
    public function setElementId($elementId)
    {
        $this->_elementId = $elementId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->_mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->_mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getIsFolder()
    {
        return $this->_isFolder;
    }

    /**
     * @param mixed $isFolder
     */
    public function setIsFolder($isFolder)
    {
        $this->_isFolder = $isFolder;
    }

    /**
     * @return mixed
     */
    public function getViewLink()
    {
        return $this->_viewLink;
    }

    /**
     * @param mixed $viewLink
     */
    public function setViewLink($viewLink)
    {
        $this->_viewLink = $viewLink;
    }

    /**
     * @return mixed
     */
    public function getDownloadLink()
    {
        return $this->_downloadLink;
    }

    /**
     * @param mixed $downloadLink
     */
    public function setDownloadLink($downloadLink)
    {
        $this->_downloadLink = $downloadLink;
    }

    /**
     * @return mixed
     */
    public function getThumbnailLink()
    {
        return $this->_thumbnailLink;
    }

    /**
     * @param mixed $thumbnailLink
     */
    public function setThumbnailLink($thumbnailLink)
    {
        $this->_thumbnailLink = $thumbnailLink;
    }

    /**
     * @return mixed
     */
    public function getIconLink()
    {
        return $this->_iconLink;
    }

    /**
     * @param mixed $iconLink
     */
    public function setIconLink($iconLink)
    {
        $this->_iconLink = $iconLink;
    }

    /**
     * @return mixed
     */
    public function getShared()
    {
        return $this->_shared;
    }

    /**
     * @param mixed $shared
     */
    public function setShared($shared)
    {
        $this->_shared = $shared;
    }

    /**
     * @return mixed
     */
    public function getStarred()
    {
        return $this->_starred;
    }

    /**
     * @param mixed $starred
     */
    public function setStarred($starred)
    {
        $this->_starred = $starred;
    }

    /**
     * @return mixed
     */
    public function getFullExtension()
    {
        return $this->_fullExtension;
    }

    /**
     * @param mixed $fullExtension
     */
    public function setFullExtension($fullExtension)
    {
        $this->_fullExtension = $fullExtension;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->_size = $size;
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return $this->_createdTime;
    }

    /**
     * @param mixed $createdTime
     */
    public function setCreatedTime($createdTime)
    {
        $this->_createdTime = $createdTime;
    }

    /**
     * @return mixed
     */
    public function getModifiedTime()
    {
        return $this->_modifiedTime;
    }

    /**
     * @param mixed $modifiedTime
     */
    public function setModifiedTime($modifiedTime)
    {
        $this->_modifiedTime = $modifiedTime;
    }

    /**
     * @return mixed
     */
    public function getOwners()
    {
        return $this->_owners;
    }

    /**
     * @param mixed $owners
     */
    public function setOwners($owners)
    {
        $this->_owners = $owners;
    }

    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->_permissions;
    }

    /**
     * @param mixed $permissions
     */
    public function setPermissions($permissions)
    {
        $this->_permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function getLastUserModifiyer()
    {
        return $this->_lastUserModifiyer;
    }

    /**
     * @param mixed $lastUserModifiyer
     */
    public function setLastUserModifiyer($lastUserModifiyer)
    {
        $this->_lastUserModifiyer = $lastUserModifiyer;
    }




}


?>