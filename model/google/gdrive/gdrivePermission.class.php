<?php 

class gdrivePermission {

	private $_id;
	private $_role;
	private $_concernedUser;

	
	public function get_id()
	{
	    return $this->_id;
	}
	 
	public function set_id($_id)
	{
	    $this->_id = $_id;
	    return $this;
	}	

	public function get_role()
	{
	    return $this->_role;
	}
	 
	public function set_role($_role)
	{
	    $this->_role = $_role;
	    return $this;
	}	

	public function get_concernedUser()
	{
	    return $this->_concernedUser;
	}
	 
	public function set_concernedUser($_concernedUser)
	{
	    $this->_concernedUser = $_concernedUser;
	    return $this;
	}	

	



}



 ?>