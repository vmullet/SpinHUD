<?php 

require(dirname(__FILE__,2).'/gSettingsManager.class.php');
require_once(dirname(__FILE__,4).'/vendor/autoload.php');

class gdriveAuthenticator {

	private static $_instance = null;

	private function __construct() { }

	public static function getInstance() {

		if (!isset(self::$_instance)) {

			self::$_instance = new gdriveAuthenticator();
		}

		return self::$_instance;

	}


	public function isTokenValid() {

		$token = gSettingsmanager::getInstance()->getSetting('gdrive_token');
		$client = new GuzzleHttp\Client();
		$statuscode = '-1';
		try {
		$res = $client->get('https://www.googleapis.com/drive/v3/files',array(
			'headers' => array(
			'Authorization' => 'Bearer '.$token
			))
		);
		$statuscode = $res->getStatusCode();
		
	}
	catch(GuzzleHttp\Exception\ClientException $e) {
		$statuscode = $e->getResponse()->getStatusCode();
	}

		if ($statuscode=='200') {
			return true;
		}
		else {
			return false;
		}

	}

	public function refreshToken() {

		$gmana = gSettingsManager::getInstance();

		$client_id = $gmana->getSetting('gclient_id');
		$client_secret = $gmana->getSetting('gclient_secret');
		$refresh_token = $gmana->getSetting('gdrive_refresh_token');

		$client = new GuzzleHttp\Client();
		
		$res= $client->post('https://accounts.google.com/o/oauth2/token', array(
		    'headers' => array(
                'Content-Type' => 'application/x-www-form-urlencoded'

            ),
            'form_params' => array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'refresh_token' => $refresh_token,
                'grant_type' => 'refresh_token'
            )
        ));

		$new_token = json_decode($res->getBody()->getContents(),true)['access_token'];

		if (isset($new_token)) {
			$gmana->updateSetting('gdrive_token',$new_token);
			return true;
		}
		else {
			return false;
		}

	}

}
