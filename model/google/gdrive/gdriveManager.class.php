<?php 

require(dirname(__FILE__,3).'/db/dbManager.class.php');

$client = new GuzzleHttp\Client();

class gdriveManager {

	private static $_instance = null;

	private function __construct() { }

	public static function getInstance() {

		if (!isset(self::$_instance)) {

			self::$_instance = new gdriveManager();
		}

		return self::$_instance;

	}

	public function cacheDrive() {

		global $client;

        $dbman = dbManager::getInstance();
		$setman = gSettingsmanager::getInstance();

		$this->resetDb($dbman);

        $this->prepareDb($dbman);

		$token = $setman->getSetting('gdrive_token');

		$file_list = $client->get('https://www.googleapis.com/drive/v3/files?pageSize=500',array(
			'headers' => array(
				'Authorization' => 'Bearer '.$token
				))
		);

        $file_list = json_decode($file_list->getBody()->getContents(),true);

        $this->insertData($file_list);

	}

	private function resetDb(dbManager $dbman) {
        $dbman->delete('spin_cache_gdrive_contain');
        $dbman->delete('spin_cache_gdrive_permissions');
        $dbman->delete('spin_cache_gdrive_files');
        $dbman->delete('spin_cache_gdrive_user');

        $dbman->query('ALTER TABLE spin_cache_gdrive_files AUTO_INCREMENT=1',false);
        $dbman->query('ALTER TABLE spin_cache_gdrive_user AUTO_INCREMENT=1',false);
        $dbman->query('ALTER TABLE spin_cache_gdrive_permissions AUTO_INCREMENT=1',false);
        $dbman->query('ALTER TABLE spin_cache_gdrive_contain AUTO_INCREMENT=1',false);
    }

    private function prepareDb(dbManager $dbman) {
        $dbman->insert('spin_cache_gdrive_user',array(
            'record_id' => '',
            'user_id' => 'my-drive',
            'fullname' => 'My Drive',
            'mail_address' => '@drive',
            'photo_link' => '#'
        ),false);

        $dbman->insert('spin_cache_gdrive_files',array(
            'record_id' => '',
            'element_id' => '-1',
            'name' => 'My Drive',
            'description' => '#',
            'mime_type' => '#',
            'is_folder' => '1',
            'view_link' => 'https://drive.google.com/drive',
            'download_link' => '#',
            'thumbnail_link' => '#',
            'icon_link' => '#',
            'shared' => '0',
            'starred' => '0',
            'full_extension' => '#',
            'created_time' => '#',
            'modified_time' => '#',
            'owner_id' => '1'
        ),false);
    }

    private function insertData($file_list) {

	    global $client;
        $setman = gSettingsManager::getInstance();
        $dbman = dbManager::getInstance();

	    $token = $setman->getSetting('gdrive_token');
	    $fields = $setman->getSetting('gdrive_fields');

	    $nextToken = -1;
	    if (isset($file_list['nextPagetoken'])) {
	        $nextToken = $file_list['nextPagetoken'];
        }

        $parent_files_ids = array();

	    $file_list = $file_list['files'];

        foreach($file_list as $raw_file) {
            $file_response = $client->get('https://www.googleapis.com/drive/v3/files/'.$raw_file['id'].'?fields='.$fields,
                array(
                    'headers' => array(
                        'Authorization' => 'Bearer '.$token
                    )
                ));

            $file_response = json_decode($file_response->getBody()->getContents(),true);

            if (!$this->userExist($file_response['owners'][0]['emailAddress'])) {
                $dbman->insert('spin_cache_gdrive_user', array(
                    'record_id' => '',
                    'user_id' => 'drive#user',
                    'fullname' => $file_response['owners'][0]['displayName'],
                    'mail_address' => $file_response['owners'][0]['emailAddress'],
                    'photo_link' => isset($file_response['owners'][0]['photoLink']) ? $file_response['owners'][0]['photoLink'] : '#'
                ), true);
            }

            $record_ids = array();

            if (isset($file_response['permissions']))
                foreach($file_response['permissions'] as $permission) {
                    if (!isset($permission['emailAddress']) || !isset($permission['displayName'])) {
                        if (!$this->userExist('anyone'))
                        $user_id = $dbman->insert('spin_cache_gdrive_user', array(
                            'record_id' => '',
                            'user_id' => 'drive#user',
                            'fullname' => 'anyone',
                            'mail_address' => 'anyone',
                            'photo_link' => isset($permission['photoLink']) ? $permission['photoLink'] : '#'
                        ), true);
                    }
                    else {
                        if (!$this->userExist($permission['emailAddress'])) {
                            $user_id = $dbman->insert('spin_cache_gdrive_user', array(
                                'record_id' => '',
                                'user_id' => 'drive#user',
                                'fullname' => $permission['displayName'],
                                'mail_address' => $permission['emailAddress'],
                                'photo_link' => isset($permission['photoLink']) ? $permission['photoLink'] : '#'
                            ), true);
                        } else

                        $user_id = $this->getUserIdByEmail($permission['emailAddress']);

                        $record_ids[] = $dbman->insert('spin_cache_gdrive_permissions', array(
                            'record_id' => '',
                            'permission_id' => $permission['id'],
                            'role' => $permission['role'],
                            'user_id' => $user_id,
                            'file_id' => 1
                        ), false);
                    }
                }



            $file_id = $dbman->insert('spin_cache_gdrive_files',array(
                'record_id' => '',
                'element_id' => $file_response['id'],
                'name' => $file_response['name'],
                'description' => isset($file_response['description']) ? $file_response['description'] : $file_response['name'],
                'mime_type' => $file_response['mimeType'],
                'is_folder' => $file_response['mimeType'] == 'application/vnd.google-apps.folder' ? 1 : 0,
                'view_link' => $file_response['webViewLink'],
                'download_link' => isset($file_response['webContentLink']) ? $file_response['webContentLink'] : '#',
                'thumbnail_link' => isset($file_response['thumbnailLink']) ? $file_response['thumbnailLink'] : '#',
                'icon_link' => $file_response['iconLink'],
                'shared' => $file_response['shared'],
                'starred' => $file_response['starred'],
                'full_extension' => explode('/',$file_response['mimeType'])[1],
                'created_time' => $file_response['createdTime'],
                'modified_time' => $file_response['modifiedTime'],
                'owner_id' => $this->getUserIdByEmail($file_response['owners'][0]['emailAddress'])
            ),false);

            if (isset($file_response['parents'])) {
                $parent_files_ids[] = $file_response['parents'][0].';'.$file_id;
            }
            else {
                $dbman->insert('spin_cache_gdrive_contain',array(
                    'container_id' => 1,
                    'content_id' => $file_id
                ),false);
            }

            foreach($record_ids as $record_id) {

                $dbman->update('spin_cache_gdrive_permissions',array(
                    'file_id' => $file_id
                ),array(
                    'WHERE record_id='.$record_id
                ));
            }

        }

        $this->linkContainerContentIds($parent_files_ids);

        if ($nextToken!=-1) {
            $nextfile_list = $client->get('https://www.googleapis.com/drive/v3/files?pageSize=500&pageToken='.$nextToken,array(
                    'headers' => array(
                        'Authorization' => 'Bearer '.$token
                    ))
            );

            $nextfile_list = json_decode($nextfile_list->getBody()->getContents(),true);
            $this->insertData($nextfile_list);
        }

    }

    private function linkContainerContentIds($parent_ids) {

	    $dbman = dbManager::getInstance();

	    foreach($parent_ids as $parent_id) {

	        $parsed = explode(';',$parent_id);
	        if (!$this->getRecordIdByElementId($parsed[0])) {
	            $container_id = $this->insertElement($parsed[0]);
            }
            else
                $container_id = $this->getRecordIdByElementId($parsed[0]);

	        $content_id = $parsed[1];

            $dbman->insert('spin_cache_gdrive_contain',array(
                'container_id' => $container_id,
                'content_id' => $content_id
            ),false);
        }

    }

	private function userExist($email_address) {
	    $result = dbManager::getInstance()->query("SELECT record_id FROM spin_cache_gdrive_user WHERE mail_address='".$email_address."'",true);

	    if (count($result)>0)
	        return $result[0]['record_id'];
	    else
	        return false;
    }

	private function getUserIdByEmail($email_address) {

        $id = dbManager::getInstance()->query("SELECT record_id FROM spin_cache_gdrive_user WHERE mail_address='".$email_address."'",true)[0]['record_id'];
        return $id;
    }

    private function getRecordIdByElementId($google_id) {

        $id = dbManager::getInstance()->query("SELECT record_id FROM spin_cache_gdrive_files WHERE element_id='".$google_id."'",true);
        if (count($id)>0)
            return $id[0]['record_id'];
        else
            return false;

    }

    private function insertElement($element_id) {
	    global $client;
	    $token = gSettingsManager::getInstance()->getSetting('gdrive_token');

        $file_response = $client->get('https://www.googleapis.com/drive/v3/files/'.$element_id,
            array(
                'headers' => array(
                    'Authorization' => 'Bearer '.$token
                )
            ));

        $file_response = json_decode($file_response->getBody()->getContents(),true);

        if ($file_response['name']!='My Drive') {
            $inserted_id = dbManager::getInstance()->insert('spin_cache_gdrive_files', array(
                'record_id' => '',
                'element_id' => $file_response['id'],
                'name' => $file_response['name'],
                'description' => '#',
                'mime_type' => $file_response['mimeType'],
                'is_folder' => '1',
                'view_link' => '#',
                'download_link' => '#',
                'thumbnail_link' => '#',
                'icon_link' => '#',
                'shared' => '0',
                'starred' => '0',
                'full_extension' => '#',
                'created_time' => '#',
                'modified_time' => '#',
                'owner_id' => '1'
            ), false);

            return $inserted_id;
        }
        else {
            dbManager::getInstance()->update('spin_cache_gdrive_files', array(
                'element_id' => $file_response['id']
            ), array(
                'WHERE record_id=1'
            ));
            return 1;
        }

    }
	

}

?>