<?php 

require('dbConnector.class.php');

class gSettingsManager {

	private static $_instance = null;

	private function __construct() { }

	public static function getInstance() {

		if (!isset(self::$_instance)) {

			self::$_instance = new gSettingsManager();
		}

		return self::$_instance;

	}

	public function getAllSettings() {

	}

	public function getSetting($setting_name) {

		$res = getConnection()->query("Select setting_value from spin_settings where setting_name='".$setting_name."'");

		$value = $res->fetchAll(PDO::FETCH_ASSOC);

		return $value[0]['setting_value'];

	}

	public function updateSetting($setting_name,$setting_value) {

		$sql = "UPDATE spin_settings SET setting_value=:setting_value where setting_name=:setting_name";
		$conn = getConnection();
		$stmt = $conn->prepare($sql);
		$stmt->bindParam(':setting_name',$setting_name);
		$stmt->bindParam(':setting_value',$setting_value);

		$result = $stmt->execute();
		if (!$result) {
			return false;
		}
		else {
			return true;
		}
	}

	

}


 ?>