<?php 

$client = new GuzzleHttp\Client();

class gmailManager {

	private static $_instance = null;

	private function __construct() { }

	public static function getInstance() {

		if (!isset(self::$_instance)) {

			self::$_instance = new gmailManager();
		}

		return self::$_instance;

	}

	public function cacheMail() {

		global $client;
		$conn = getConnection();

		$conn->query('DELETE FROM spin_cache_gmail');
		$conn->query('DELETE FROM spin_cache_gmail_attachements');

		$token = gSettingsmanager::getInstance()->getSetting('gtoken');
		$res = $client->get('https://www.googleapis.com/gmail/v1/users/me/messages?labelIds=INBOX&maxResults=100&q=in:inbox -category:{social promotions forums}',array(
			'headers' => array(
				'Authorization' => 'Bearer '.$token
				))
		);

		$content = json_decode($res->getBody()->getContents(),true)['messages'];
		foreach($content as $mail_id) {
			$res = $client->get('https://www.googleapis.com/gmail/v1/users/me/messages/'.$mail_id['id'],array(
				'headers' => array(
					'Authorization' => 'Bearer '.$token
					))

			);

			$raw_mail = json_decode($res->getBody()->getContents(),true);

			$sender = '';
			$receiver = '';
			$date = '';
			$subject = '';
			$content = '';

			foreach($raw_mail['payload']['headers'] as $header) {

				if ($header['name']=='From')
					$sender = utf8_encode($header['value']);

				if ($header['name']=='To')
					$receiver = utf8_encode($header['value']);

				if ($header['name']=='Date') 
					$date = (new DateTime($header['value']))->format('Y-m-d H:i:s');

				if ($header['name']=='Subject')
					$subject = utf8_encode($header['value']);

			}
			
			if (isset($raw_mail['payload']['body']['data'])) {
				$content = $raw_mail['payload']['body']['data'];
			} else {
				$content = '';
			}

			$content = str_replace('+','-',$content);
			$content = str_replace('/','_',$content);

			$sql = "INSERT INTO spin_cache_gmail VALUES('',:mail_id,:mail_sender,:mail_receiver,:mail_date,:mail_subject,:mail_snippet,:mail_content)";
			$stmt = $conn->prepare($sql);
			$stmt->bindParam(':mail_id',$raw_mail['id']);
			$stmt->bindParam(':mail_sender',$sender);
			$stmt->bindParam(':mail_receiver',$receiver);
			$stmt->bindParam(':mail_date',$date);
			$stmt->bindParam(':mail_subject',$subject);
			$stmt->bindParam(':mail_snippet',$raw_mail['snippet']);
			$stmt->bindParam(':mail_content',$content);
			$stmt->execute();
			
			$lastInsertId = $conn->lastInsertId();
			$this->cacheAttachements($raw_mail,$lastInsertId);
			
		}

	}


	public function cacheAttachements($raw_mail,$last_id) {

		if (isset($raw_mail['payload']['parts'])) {
			$this->check_parts($raw_mail['id'],$raw_mail['payload']['parts'],$last_id);
		}

	}

	public function check_parts($mail_id,$parts_array,$last_id) {

		global $client;

		$conn = getConnection();

		$token = gSettingsmanager::getInstance()->getSetting('gtoken');

		$mimeType = '';

		if (is_array($parts_array))
			foreach($parts_array as $part) {
				$mimeType = $part['mimeType'];
				if (isset($part['body']['attachmentId'])) {
					
					$headers = '';

					foreach($part['headers'] as $header) {
						$headers.=$header['name'].'::'.$header['value'].';';
					}
					$filename = $part['filename'];
					$id_att = $part['body']['attachmentId'];
					$size = $part['body']['size'];
					$content = '';
					$res = $client->get('https://www.googleapis.com/gmail/v1/users/me/messages/'.$mail_id.'/attachments/'.$id_att,array(
						'headers' => array(
							'Authorization' => 'Bearer '.$token
							))

					);

					$content = json_decode($res->getBody()->getContents(),true)['data'];
					$content = str_replace('+','-',$content);$content = str_replace('/','_',$content);

					$sql = "INSERT INTO spin_cache_gmail_attachements VALUES('',:attachement_id,:mime_type,:headers,:size,:content,:mail_id)";
					$stmt = $conn->prepare($sql);
					$stmt->bindParam(':attachement_id',$id_att);
					$stmt->bindParam(':mime_type',$mimeType);
					$stmt->bindParam(':headers',$headers);
					$stmt->bindParam(':size',$size);
					$stmt->bindParam(':content',$content);
					$stmt->bindParam(':mail_id',$last_id);
					$stmt->execute();

				}

				$content = '';

				if ($mimeType=='text/html') {
					$content = $part['body']['data'];
				} else if ($mimeType=='text/plain') {
					$content = $part['body']['data'];
				}

				$sql = "UPDATE spin_cache_gmail SET content=:content where id=:last_id";
				$stmt = $conn->prepare($sql);
				$stmt->bindParam(':content',$content);
				$stmt->bindParam(':last_id',$last_id);
				$stmt->execute();

				if (isset($part['parts']))
				$this->check_parts($mail_id,$part['parts'],$last_id);

			}


		}



	}



	?>