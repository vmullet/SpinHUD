<?php 

class gmailAttachment {

    private $_id;
	private $_attachmentId;
	private $_mimeType;
	private $_headers;
	private $_fileName;
	private $_size;
	private $_data;

    /**
     * attachement constructor.
     * @param $_id
     * @param $_attachementId
     * @param $_mimeType
     * @param $_headers
     * @param $_fileName
     * @param $_size
     * @param $_data
     */
    public function __construct($_id, $_attachementId, $_mimeType, $_headers, $_fileName, $_size, $_data)
    {
        $this->_id = $_id;
        $this->_attachmentId = $_attachementId;
        $this->_mimeType = $_mimeType;
        $this->_headers = $_headers;
        $this->_fileName = $_fileName;
        $this->_size = $_size;
        $this->_data = $_data;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->_mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->_mimeType = $mimeType;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @param mixed $headers
     */
    public function setHeaders($headers)
    {
        $this->_headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->_fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
    }

    /**
     * @return mixed
     */
    public function getAttachmentId()
    {
        return $this->_attachmentId;
    }

    /**
     * @param mixed $attachmentId
     */
    public function setAttachmentId($attachmentId)
    {
        $this->_attachmentId = $attachmentId;
    }

    /**
     * @return mixed
     */
    public function getSize($unit)
    {
        switch($unit) {
            case 'kb':
                return round($this->_size/pow(1024,1),2);
                break;
            case 'mb':
                return round($this->_size/pow(1024,2),2);
                break;
            case 'gb':
                return round($this->_size/pow(1024,3),2);
                break;
        }
        return $this->_size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->_size = $size;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }



}


?>