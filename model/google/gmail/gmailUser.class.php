<?php 

class user {

	private $_userId;
	private $_mailAddress;
	private $_profilePicture;
	private $_messagesTotal;

	public function get_userId()
	{
	    return $this->_userId;
	}
	 
	public function set_userId($_userId)
	{
	    $this->_userId = $_userId;
	    return $this;
	}	

	public function get_mailAddress()
	{
	    return $this->_mailAddress;
	}
	 
	public function set_mailAddress($_mailAddress)
	{
	    $this->_mailAddress = $_mailAddress;
	    return $this;
	}	

	public function get_profilePicture()
	{
	    return $this->_profilePicture;
	}
	 
	public function set_profilePicture($_profilePicture)
	{
	    $this->_profilePicture = $_profilePicture;
	    return $this;
	}	

	public function get_messagesTotal()
	{
	    return $this->_messagesTotal;
	}
	 
	public function set_messagesTotal($_messagesTotal)
	{
	    $this->_messagesTotal = $_messagesTotal;
	    return $this;
	}	

	
}


 ?>