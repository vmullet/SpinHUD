<?php 

class mail {

	private $_sender;
	private $_receiver;
    private $_date;
    private $_subject;
    private $_snippet;
	private $_content;
	private $_attachements;

    public function get_sender()
    {
        return $this->_sender;
    }
    
    public function set_sender($_sender)
    {
        $this->_sender = $_sender;
        return $this;
    }

    public function get_receiver()
    {
        return $this->_receiver;
    }
    
    public function set_receiver($_receiver)
    {
        $this->_receiver = $_receiver;
        return $this;
    }

    public function get_date()
    {
        return $this->_date;
    }
     
    public function set_date($_date)
    {
        $this->_date = $_date;
        return $this;
    }   

    public function get_subject()
    {
        return $this->_subject;
    }
     
    public function set_subject($_subject)
    {
        $this->_subject = $_subject;
        return $this;
    }   

    public function get_snippet()
    {
        return $this->_snippet;
    }
     
    public function set_snippet($_snippet)
    {
        $this->_snippet = $_snippet;
        return $this;
    }   

    public function get_content()
    {
        return $this->_content;
    }
    
    public function set_content($_content)
    {
        $this->_content = $_content;
        return $this;
    }

    public function get_attachements()
    {
        return $this->_attachements;
    }
    
    public function set_attachements($_attachements)
    {
        $this->_attachements = $_attachements;
        return $this;
    }


}


?>