<?php 

class attachement {

	private $_attachementId;
	private $_size;
	private $_data;


    public function get_attachementId()
    {
        return $this->_attachementId;
    }
    
    public function set_attachementId($_attachementId)
    {
        $this->_attachementId = $_attachementId;
        return $this;
    }

    public function get_size()
    {
        return $this->_size;
    }
    
    public function set_size($_size)
    {
        $this->_size = $_size;
        return $this;
    }	

    public function get_data()
    {
        return $this->_data;
    }
    
    public function set_data($_data)
    {
        $this->_data = $_data;
        return $this;
    }	



}


?>