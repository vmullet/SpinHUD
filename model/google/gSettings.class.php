<?php 

class gSettings {

	private $_mailAddress;
	private $_token;
	private $_refreshToken;
	
	public function get_mailAddress()
	{
	    return $this->_mailAddress;
	}
	 
	public function set_mailAddress($_mailAddress)
	{
	    $this->_mailAddress = $_mailAddress;
	    return $this;
	}	

	public function get_token()
	{
	    return $this->_token;
	}
	 
	public function set_token($_token)
	{
	    $this->_token = $_token;
	    return $this;
	}	

	public function get_refreshToken()
	{
	    return $this->_refreshToken;
	}
	 
	public function set_refreshToken($_refreshToken)
	{
	    $this->_refreshToken = $_refreshToken;
	    return $this;
	}	


}


 ?>