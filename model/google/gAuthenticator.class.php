<?php 

require('gSettingsManager.class.php');
require_once(dirname(__FILE__,3).'/vendor/autoload.php');

class gAuthenticator {

	private static $_instance = null;

	private function __construct() { }

	public static function getInstance() {

		if (!isset(self::$_instance)) {

			self::$_instance = new gAuthenticator();
		}

		return self::$_instance;

	}


	public function isTokenValid() {

		$token = gSettingsmanager::getInstance()->getSetting('gmail_token');
		$client = new GuzzleHttp\Client();
		$statuscode = '-1';
		try {
		$res = $client->get('https://www.googleapis.com/gmail/v1/users/me/messages',array(
			'headers' => array(
			'Authorization' => 'Bearer '.$token
			))
		);
		$statuscode = $res->getStatusCode();
		
	}
	catch(GuzzleHttp\Exception\ClientException $e) {
		$statuscode = $e->getResponse()->getStatusCode();
	}

		

		if ($statuscode=='200') {
			return true;
		}
		else {
			return false;
		}

	}

	public function refreshToken() {

		$gmana = gSettingsManager::getInstance();

		$client_id = $gmana->getSetting('gclient_id');
		$client_secret = $gmana->getSetting('gclient_secret');
		$refresh_token = $gmana->getSetting('gmail_refresh_token');

		$client = new GuzzleHttp\Client();
		
		$res= $client->post('https://www.googleapis.com/oauth2/v4/token?grant_type=refresh_token&client_id='.$client_id.'&client_secret='.$client_secret.'&refresh_token='.$refresh_token);

		$new_token = json_decode($res->getBody()->getContents(),true)['access_token'];
		if (isset($new_token)) {
			$gmana->updateSetting('gtoken',$new_token);
			return true;
		}
		else {
			return false;
		}

	}

}


 ?>