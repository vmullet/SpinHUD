<?php

class dbManager
{

    private static $_instance = null;

    const DB_HOST = "localhost";
    const DB_NAME = "spin_db";
    const DB_USER = "root";
    const DB_PASS = "";

    private function __construct()
    {
    }

    public static function getInstance()
    {

        if (!isset(self::$_instance)) {
            self::$_instance = new dbManager();
        }

        return self::$_instance;


    }

    public function getConnection()
    {

        $conn = new PDO("mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_NAME, self::DB_USER, self::DB_PASS);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;

    }

    public function select($table_name,$fields=array()) {

        $conn = $this->getConnection();
        $results = null;
        $sql = '';
        if (count($fields)==0) {
           $sql = "SELECT * FROM ".$table_name;
        } else {
            $sql = "SELECT ";
            $cnt = 0;
            foreach($fields as $field) {
                $sql.= $field." ";
                if ($cnt<count($fields)-1)
                    $sql.=",";
                $cnt++;
            }
            $sql.=" FROM ".$table_name;
        }

        $res = $conn->query($sql);
        $res = $res->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }

    public function insert($table_name,$fields_values=array(),$ignore) {

        $conn = $this->getConnection();
        if ($ignore)
            $sql = "INSERT IGNORE INTO ".$table_name;
        else
            $sql = "INSERT INTO ".$table_name;

        if (count($fields_values)>0) {
            $fields = '('.implode(',',array_keys($fields_values)).') ';
        }
        else {
            $fields = ' ';
        }
        $kvalues = "VALUES(:".implode(',:',array_keys($fields_values)).')';
        $sql.=$fields.' '.$kvalues;

        $stmt = $conn->prepare($sql);
        foreach($fields_values as $key=>$value) {
            $stmt->bindValue(':'.$key,$value);
        }
        $stmt->execute();
        return $conn->lastInsertId();
        
    }

    public function update($table_name,$values,$criteria_list) {
        $conn = $this->getConnection();
        $sql = "UPDATE ".$table_name." SET ";
        $cnt = 0;
        foreach($values as $key=>$value) {
            $sql.=$key."=:".$key;
            $cnt++;
            if ($cnt!=count($value))
                $sql.=",";

        }
        $cnt = 0;
        foreach($criteria_list as $criteria) {
            $sql.=" ".$criteria;
            $cnt++;
            if ($cnt!=count($criteria))
                $sql.=" and ";

        }

        $stmt = $conn->prepare($sql);
        foreach($values as $key=>$value) {
            $stmt->bindValue(':'.$key,$value);
        }
        $stmt->execute();

    }

    public function delete($table_name) {
        $conn = $this->getConnection();
        $conn->query('DELETE FROM '.$table_name);
    }

    public function query($sql,$fetch) {
        $conn = $this->getConnection();
        $res = $conn->query($sql);
        if ($fetch)
        $res = $res->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }


}


?>